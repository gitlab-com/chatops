# frozen_string_literal: true

module Chatops
  module Gitlab
    class AutoDeploy
      TASK_PROJECT = 'gitlab-org/release/tools'
      RELEASE_TOOLS_BOT_USERNAME = 'gitlab-release-tools-bot'
      FEATURE_FLAG = 'do_not_auto_create_new_deployment_pipelines'

      def initialize(client = nil)
        @client = client
      end

      def tasks
        @tasks ||= @client
          .pipeline_schedules(TASK_PROJECT)
          .select { |s| s.description.start_with?('auto_deploy:') }
      end

      def pause
        tasks = update_tasks(active: false)

        # Make feature flag active so that new deployment pipelines are not created
        update_feature_flag(active: true)

        tasks
      end

      def unpause_prepare
        pipeline_schedule_take_ownership(prepare_task)

        @client
          .edit_pipeline_schedule(TASK_PROJECT, prepare_task.id, active: true)
      end

      def pause_prepare
        pipeline_schedule_take_ownership(prepare_task)

        @client
          .edit_pipeline_schedule(TASK_PROJECT, prepare_task.id, active: false)
      end

      def unpause
        tasks = update_tasks(active: true)

        # Make feature flag inactive so that new deployment pipelines are created
        update_feature_flag(active: false)

        tasks
      end

      def update_tasks(attrs = {})
        ensure_tasks!

        tasks.map! do |task|
          pipeline_schedule_take_ownership(task)

          @client.edit_pipeline_schedule(TASK_PROJECT, task.id, **attrs)
        end
      end

      private

      def pipeline_schedule_take_ownership(task)
        return if task.owner.username == RELEASE_TOOLS_BOT_USERNAME

        @client.pipeline_schedule_take_ownership(TASK_PROJECT, task.id)
      end

      def prepare_task
        ensure_tasks!

        task = tasks.find { |t| t.description == 'auto_deploy:prepare' }

        raise "No prepare task was found in #{TASK_PROJECT}" unless task

        task
      end

      def ensure_tasks!
        return if tasks.any?

        raise "No auto_deploy tasks found in #{TASK_PROJECT}"
      end

      def update_feature_flag(attrs)
        response = @client.feature_flag(TASK_PROJECT, FEATURE_FLAG)

        return unless attrs.any? { |key, val| val != response[key.to_s] }

        attrs = attrs.merge(description: "Updated by #{ENV['CI_JOB_URL']}")
        @client.edit_feature_flag(TASK_PROJECT, FEATURE_FLAG, **attrs)
      end
    end
  end
end
